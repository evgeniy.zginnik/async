class DoAsync {

  #regCallBackAfter = [];
  #regCallBackError = [];

  constructor(func) {
    let count = true;
    let currentArg;
    this.done = (arg) => {
      if(count) {
        count = !count
        setTimeout(() => this.#regCallBackAfter.forEach(i => {
          if(currentArg) {
            currentArg = i(currentArg)
            return
          }
          currentArg = i(arg)
        }), 0)
      }
    };

    this.fail = (arg) => {
      if(count) {
        count = !count
        setTimeout(() => this.#regCallBackError.forEach(i => {
          if(currentArg) {
            currentArg = i(currentArg)
            return
          }
          currentArg = i(arg)
        }), 0)
      }
    };

    func(this.done, this.fail);
  }

  after(arg) {
    this.#regCallBackAfter.push(arg);
    return this;
  };

  error(arg) {
    this.#regCallBackError.push(arg);
    return this;
  }
}

// <------------------------------

const users = [{ name: 'Jack', name: 'Masha' }];

const getUser = new DoAsync(function(done, fail) {
  console.log(1);
  done(users);
  console.log(2);
  fail('Some Error');
  console.log(3);
  done(null);
})

console.log(4);

getUser.after(function(res) {
  console.log(8, res);
});

console.log(5);

getUser.error(function(err) {
  console.log('it has never been called!', err);
});

console.log(6);

getUser.after(function(res) {
  console.log(9, res);
});

console.log(7);

getUser
  .after(function(res) {
    console.log(9.1, res);
  return 10;
  })
  .after(function(res) {
    console.log(9.2, res);
  });

getUser
  .after(function(res) {
    console.log(10, res);
  })